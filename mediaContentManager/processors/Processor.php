<?php

namespace reseed\mediaContentManager\processors;

use reseed\mediaContentManager\components\BaseStorage;
use yii\base\Configurable;

/**
 * Class Processor
 *
 * @author Ivan <frostealth@frostealth.ru>
 * @since 1.0
 *
 * @package reseed\mediaContentManager\processors
 */
class Processor implements Configurable
{
    /** @var BaseStorage */
    protected $storage;

    /** @var string */
    protected $filename;

    /**
     * @var bool Do some process async.
     *           For example, you can create thumbnails async (in background).
     */
    protected $async;

    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.04.05
     * @access public
     *
     * @return string
     */
    public static function className()
    {
        return get_called_class();
    }

    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.04.10
     * @access public
     *
     * @param string $filename
     * @param bool $async
     * @param BaseStorage $storage
     */
    public function __construct($filename, $async, BaseStorage $storage)
    {
        $this->filename = $filename;
        $this->storage = $storage;
        $this->async = $async;
    }

    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.04.10
     * @access public
     *
     * @param string $filename
     */
    public function upload($filename)
    {
        $this->storage->upload(
            $this->getFilename(),
            file_get_contents($filename)
        );
    }

    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.04.10
     * @access public
     */
    public function delete()
    {
        $this->storage->delete($this->getFilename());
    }

    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.04.10
     * @access public
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->storage->getUrl($this->getFilename());
    }

    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.04.10
     * @access public
     *
     * @return string
     */
    protected function getFilename()
    {
        return $this->filename;
    }

    /**
     * @author Andreev <andreev1024@gmail.com>
     * @return BaseStorage
     */
    public function getStorage()
    {
        return $this->storage;
    }
}
