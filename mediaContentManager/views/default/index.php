<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use kartik\grid\ActionColumn;
use reseed\reWidgets\rebox\ReBox;
use reseed\mediaContentManager\models\File;
use reseed\reWidgets\realert\ReAlert;

/**
 * @var \yii\web\View $this
 */

$this->title = Yii::$app->translate->t('files', 'rereca-app');
$this->params['breadcrumbs'] = [
    [
        'label' => Yii::$app->translate->t('media content manager', 'rereca-app'), 
        'url' => ['default/index']
    ],
    $this->title,
];

echo ReAlert::widget();
?>

<div>
    <?php ReBox::begin([
        'header' => [
            'options' => [
                'class' => 'box-name',
                'title' => $this->title
            ],
            'icon' => [
                'name' => 'file-text',
                'framework' => 'fa',
                'options' => [],
                'space' => true,
                'tag' => 'i'
            ],
        ],
    ]); ?>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'original_name',
            'created_at',
            [
                'attribute' => 'name',
                'label' => Yii::$app->translate->t('url', 'rereca-app'),
                'value' => function (File $data) {
                    return Html::a(
                        Yii::$app->translate->t('copy to clipboard', 'rereca-app'), 
                        '', 
                        [
                            'class' => 'btn btn-primary copy',
                            'data-target' => 'modalFieFrame',
                            'data-toggle' => 'modal',
                            'data-file' => $data->getUrl(),
                            'title' => Yii::$app->translate->t('copy to clipboard', 'rereca-app')
                        ]
                    );
                },
                'width' => '10%',
                'format' => 'raw'
            ],
            [
                'class' => ActionColumn::className(),
                'controller' => 'files',
                'header' => ''
            ]
        ]
    ]) ?>

    <?php ReBox::end(); ?>
</div>

<!-- Begin modal copy url to clipboard -->
<?php 
    Modal::begin([
        'header' => Html::tag('h3', Yii::$app->translate->t('file url', 'rereca-app')),
        'toggleButton' => false,
        'id' => 'modalFieFrame',
    ]);
?>

<div class="row">
    <div class="col-sm-12 col-xs-12">
        <div class="form-group field-systemcurrency-name required">
            <?= Html::input('text', 'url', '', ['class' => 'form-control copyText']) ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group col-sm-12 col-xs-12 text-center">
        <?= Html::button(
            Yii::$app->translate->t('copy', 'rereca-app'), 
            [
                'id' => 'btnCopyUrl', 
                'class' => 'btn btn-success',
                'style' => 'width: 30%'
            ]
        ) ?>
    </div>
</div>

<?php
    Modal::end();
?>
<!-- End modal copy url to clipboard -->

<?php
$successMessage = Yii::$app->translate->t('copy url successful', 'rereca-app');
$failMessage = Yii::$app->translate->t('copy url unsuccessful', 'rereca-app');

$this->registerJs(<<<JS
$(document).on("click", ".copy", function() {
    $("input[name=\"url\"]").val($(this).data("file"));
    $("#modalFieFrame").modal();
});

$(document).on("click", "#btnCopyUrl", function() {
    $("input[name=\"url\"]").select();

    try {
        var successful = document.execCommand('copy');

        if (successful) {
            $("#modalFieFrame").trigger("click");
            new ReAlert({
                message: "$successMessage",
                alert_type: "success",
                width : "300px",
            }).show();
        } else {
            new ReAlert({
                message: "$failMessage",
                alert_type: "danger",
                width : "300px",
            }).show();
        }
    } catch (error) {
        console.log(error);
    }
});
JS
);
?>