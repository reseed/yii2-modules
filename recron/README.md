#Cron module.

This module provides a GUI for CRON commands management in Reseed web app. 

##Installation

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Add

```
{
  "type": "git",
  "url": "git@bitbucket.org:reseed/recron.git"
},
```

to the `repositories` section of your `composer.json` and

```
"reseed/recron": "*",
```

to the `require` section of your `composer.json` file.

##Configuration

- Add module to backend config file

```
'modules' => [
    'cron' => [
        'class' => 'reseed\recron\Module'
    ]
]
```

- If you want to allow access in _frontend_ (with token) you must add module to _frontend_ config file

```
'modules' => [
    'cron' => [
        'class' => 'reseed\recron\Module'
    ]
]
```

## Migrations:

At first time when you access CRON via web application it creates table re_cron

## How to use:

- Create new cron task at settings/index page in cron module
- After that in settings/apply module will fill crontab automatically