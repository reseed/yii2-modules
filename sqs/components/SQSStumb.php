<?php
/**
 * Rebecca Application
 *
 * @license    MIT
 * @author     Andreev <andreev1024@gmail.com>
 * @copyright  2015-11-02
 * @link       https://bitbucket.org/reseed/rebecca
 * @version    1.1
 */
namespace reseed\sqs\components;

/**
 * SQS stumb class (null object pattern).
 */
class SQSStumb implements SQSInterface
{
    /**
     * @inheritdoc
     */
    public function sendMessage($message, array $options)
    {
        // do nothing
    }

    /**
     * @inheritdoc
     */
    public function sendMessageBatch(array $messages, array $options)
    {
        // do nothing
    }
}
