<?php
/**
 * Rebecca Application
 *
 * @license    MIT
 * @author     Andreev <andreev1024@gmail.com>
 * @copyright  2015-11-02
 * @link       https://bitbucket.org/reseed/rebecca
 * @version    1.1
 */
namespace reseed\sqs;

/**
 * Module class for SQS component.
 * @package reseed\sqs
 */
class Module extends \yii\base\Module
{
    /**
     * translate category for i18n
     * @var string
     */
    public static $translateCategory = 'sqs';

    public $defaultRoute = 'error';

    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}
